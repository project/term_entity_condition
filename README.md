## Introduction

This module provides a block visibility condition that allows blocks to be placed on specific canonical taxonomy term
pages.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install with composer: `composer require drupal/term_entity_condition` then enable the module.

## Using

After installing and enabling this module, edit any block configuration to place that block only on a specific taxonomy
term page.


## Maintainers

- Chris Snyder - https://www.drupal.org/u/ChrisSnyder
