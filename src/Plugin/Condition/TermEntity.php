<?php

declare(strict_types=1);

namespace Drupal\term_entity_condition\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Term Entity' condition plugin.
 *
 * Controls block visibility rules based the current taxonomy term.
 *
 * @Condition(
 *   id = "term_entity",
 *   label = @Translation("Taxonomy Term"),
 *   context_definitions = {
 *     "taxonomy_term" = @ContextDefinition("entity:taxonomy_term", required = FALSE, label = @Translation("Taxonomy Term"))
 *   }
 * )
 */
class TermEntity extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Term config key.
   *
   * @var string
   */
  protected const TERM_CONFIG_KEY = 'term_uuids';

  /**
   * Creates a new Term Entity condition instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['terms'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Taxonomy term(s)'),
      '#default_value' => $this->loadTerms(),
      '#target_type' => 'taxonomy_term',
      '#tags' => TRUE,
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      static::TERM_CONFIG_KEY => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $uuids = [];

    foreach ($form_state->getValue('terms') ?? [] as $term) {
      $uuids[] = $this->entityTypeManager
        ->getStorage('taxonomy_term')
        ->load($term['target_id'])
        ->uuid();
    }
    $this->configuration[static::TERM_CONFIG_KEY] = $uuids;

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    if (empty($this->configuration[static::TERM_CONFIG_KEY]) && !$this->isNegated()) {
      return TRUE;
    }

    $taxonomyTerm = $this->getContextValue('taxonomy_term');
    if (!$taxonomyTerm instanceof TermInterface) {
      return FALSE;
    }

    return in_array($taxonomyTerm->uuid(), $this->configuration[static::TERM_CONFIG_KEY], TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    $terms = $this->loadTerms();
    $termNames = [];
    foreach ($terms as $term) {
      $termNames[] = $term->getName();
    }

    return $this->t('The current page is @not one of the following taxonomy term(s): @terms.', [
      '@terms' => implode(", ", $termNames),
      '@not' => $this->isNegated() ? 'not' : '',
    ]);
  }

  /**
   * Load terms referenced in configuration.
   *
   * @return \Drupal\taxonomy\TermInterface[]
   *   Term Entity from configuration.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadTerms(): array {
    $terms = [];

    if (!empty($this->configuration[static::TERM_CONFIG_KEY])) {
      $terms = $this->entityTypeManager
        ->getStorage('taxonomy_term')
        ->loadByProperties(['uuid' => $this->configuration[static::TERM_CONFIG_KEY]]);
    }

    return $terms;
  }

}
